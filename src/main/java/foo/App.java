package foo;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws UnknownHostException
    {
        System.out.println( "Hello World!" );
        
     // To directly connect to a single MongoDB server (note that this will not auto-discover the primary even
     // if it's a member of a replica set:
     MongoClient mongoClient = new MongoClient();
     // or
     //MongoClient mongoClient = new MongoClient( "localhost" );
     // or
     //MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
     // or, to connect to a replica set, with auto-discovery of the primary, supply a seed list of members
     //MongoClient mongoClient = new MongoClient(Arrays.asList(new ServerAddress("localhost", 27017),
     //                                      new ServerAddress("localhost", 27018),
     //                                      new ServerAddress("localhost", 27019)));

     DB db = mongoClient.getDB( "test" );
     
     DBCollection coll = db.getCollection("disney");
     
     BasicDBObject doc = new BasicDBObject("name", "Abu")
     	.append("animal", "Monkey")
     	.append("movie", new BasicDBObject("title", "Aladdin"));
     coll.insert(doc);
     
     //prints out first document
     //DBObject myDoc = coll.findOne();
     //System.out.println(myDoc);
     
     DBCursor cursor = coll.find();
     //prints out all documents in the collection
     //try{
     //	 while(cursor.hasNext()){
     //		 System.out.println(cursor.next());
     //	 }
     //} finally {
     //	 cursor.close();
     //}
     
     BasicDBObject query = doc;

     cursor = coll.find(query);
     //prints out query for new document that was added
     try {
        while(cursor.hasNext()) {
            System.out.println(cursor.next());
        }
     } finally {
        cursor.close();
     }
     
    }
}
